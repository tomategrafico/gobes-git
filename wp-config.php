<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'gobes-git' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'ewK^Sg.=Bq1mvM/QBx8fEyBq?KR}=k/az:dkb?S-]l5Ww@O<2AMt)[K5~Cu(K|VT' );
define( 'SECURE_AUTH_KEY',  'GrPv@85)x[$Flt)5z*07Et[M<?Q3Hn6^;K6=DZn.qQNw;)5FX%zW$|r o&urVj:+' );
define( 'LOGGED_IN_KEY',    'VS&FE&y;OlRxHK}>;+,K/r:))XII}}D6y`N*()}1m!4N),Yds^XySxgT33hNi/F7' );
define( 'NONCE_KEY',        'G7h0JaQzf@&-&q=>A[P;Xn-z 9M5vEQ|~:`(DCG&4Ch&#4hD?Sbit9:a4@yDwt/`' );
define( 'AUTH_SALT',        'b/k;M]m!$[du*kf2hz8fPL2WzJoChjKxk$HMxFZ4$S Vo>~Egc#yP/!Ur Q4dpsz' );
define( 'SECURE_AUTH_SALT', 'tW:ng{sEVqM7Nsw1e;]B>3]9l1oBl VItI^)nK4#S$r195VP(W?zk=^&LQr <i6.' );
define( 'LOGGED_IN_SALT',   'P}!|M! {+n;IM2qpkbV:1` 3zzobVuiSs07GY(aiDv)Rn]%mZ?mCzkd_{O+L I@0' );
define( 'NONCE_SALT',       'u7-<<XFm0=~4.b;)9nRQ=8Bc}4~RV|7S Lw5*Cnz{&)*G?a?n^-02L96kQWUHFg[' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
